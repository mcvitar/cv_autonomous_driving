from tensorflow.keras.models import load_model
import cv2
from skimage import exposure
import numpy as np


class TrafficSignClassification:
    model = None
    labelNames = None

    @staticmethod
    def get_model():
        if TrafficSignClassification.model is None:
            TrafficSignClassification.model = load_model("resources/igrbas/trafficsignnetcv.model")
        return TrafficSignClassification.model

    @staticmethod
    def get_label_names():
        if TrafficSignClassification.labelNames is None:
            TrafficSignClassification.labelNames = open("resources/igrbas/signnames.csv").read().strip().split("\n")[1:]
            TrafficSignClassification.labelNames = [l.split(",")[1] for l in TrafficSignClassification.labelNames]
        return TrafficSignClassification.labelNames

    @staticmethod
    def predict(frame, x, y, width, height):
        model = TrafficSignClassification.get_model()
        labelNames = TrafficSignClassification.get_label_names()

        sign = frame[y:(y+height), x:(x+width)]

        sign = cv2.resize(sign, (32, 32))
        sign = exposure.equalize_adapthist(sign, clip_limit=0.1)

        sign = sign.astype("float32") / 255.0
        sign = np.expand_dims(sign, axis=0)

        prediction = model.predict(sign)
        j = prediction.argmax(axis=1)[0]
        label = labelNames[j]

        if prediction[0][j] < 0.9:
            return ""

        return label


def write_sign_text(frame, text, x, y):
    cv2.putText(frame, text, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)
