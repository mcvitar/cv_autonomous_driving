## Just follow the steps below and TO-DO comments in project.
1. Define a new branch named like feature you are adding.
2. Create your module named like email.py and implement proper behavior.
3. If you need to install python package via pip, add it to requirements.txt (Reason for step 3.: https://realpython.com/lessons/using-requirement-files)
4. If you have extra dependency in form of resources (xml, json, images, etc.) add it to resources/email-alias (For example: resources/ajavor/sample.txt)
5. Add video files to resources/video for testing, we do not want them on GitLab, so they are in .gitignore file
6. Call functions as stated in main.py.
7. Commit all changes.
8. Open Pull request and make a description of your contribution, explain everything in detail.
