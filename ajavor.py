import cv2 as cv


def detect_circle(frame):
    # This is sample detection function finding center of image.
    (h, w) = frame.shape[:2]
    return h//2, w//2


def draw_circle(frame, center_x, center_y):
    # This is sample drawing function that takes frame and extra parameters for center
    cv.circle(frame, (center_y, center_x), 10, (0, 0, 255), -1)

