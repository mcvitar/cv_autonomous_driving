import cv2
import pickle
import numpy as np
from keras.models import load_model

#############################################

frameWidth = 640  # CAMERA RESOLUTION
frameHeight = 480
brightness = 180

##############################################

# SET UP THE VIDEO CAMERA
cap = cv2.VideoCapture("DayDrive2.mp4")
cap.set(3, frameWidth)
cap.set(4, frameHeight)
cap.set(10, brightness)

# IMPORT THE TRAINED MODEL
model = load_model('trained_model.h5')

def grayscale(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return img

def equalize(img):
    img = cv2.equalizeHist(img)
    return img

def preprocessing(img):
    img = grayscale(img)
    img = equalize(img)
    img = img / 255
    return img

def getCalssName(sign):
    if sign == 0:
        return 'Speed Limit 20 km/h'
    elif sign == 1:
        return 'Speed Limit 30 km/h'
    elif sign == 2:
        return 'Speed Limit 50 km/h'
    elif sign == 3:
        return 'Speed Limit 60 km/h'
    elif sign == 4:
        return 'Speed Limit 70 km/h'
    elif sign == 5:
        return 'Speed Limit 80 km/h'
    elif sign == 6:
        return 'End of Speed Limit 80 km/h'
    elif sign == 7:
        return 'Speed Limit 100 km/h'
    elif sign == 8:
        return 'Speed Limit 120 km/h'
    elif sign == 9:
        return 'No passing'
    elif sign == 10:
        return 'No passing for vechiles over 3.5 metric tons'
    elif sign == 11:
        return 'Right-of-way at the next intersection'
    elif sign == 12:
        return 'Priority road'
    elif sign == 13:
        return 'Yield'
    elif sign == 14:
        return 'Stop'
    elif sign == 15:
        return 'No vechiles'
    elif sign == 16:
        return 'Vechiles over 3.5 metric tons prohibited'
    elif sign == 17:
        return 'No entry'
    elif sign == 18:
        return 'General caution'
    elif sign == 19:
        return 'Dangerous curve to the left'
    elif sign == 20:
        return 'Dangerous curve to the right'
    elif sign == 21:
        return 'Double curve'
    elif sign == 22:
        return 'Bumpy road'
    elif sign == 23:
        return 'Slippery road'
    elif sign == 24:
        return 'Road narrows on the right'
    elif sign == 25:
        return 'Road work'
    elif sign == 26:
        return 'Traffic signals'
    elif sign == 27:
        return 'Pedestrians'
    elif sign == 28:
        return 'Children crossing'
    elif sign == 29:
        return 'Bicycles crossing'
    elif sign == 30:
        return 'Beware of ice/snow'
    elif sign == 31:
        return 'Wild animals crossing'
    elif sign == 32:
        return 'End of all speed and passing limits'
    elif sign == 33:
        return 'Turn right ahead'
    elif sign == 34:
        return 'Turn left ahead'
    elif sign == 35:
        return 'Ahead only'
    elif sign == 36:
        return 'Go straight or right'
    elif sign == 37:
        return 'Go straight or left'
    elif sign == 38:
        return 'Keep right'
    elif sign == 39:
        return 'Keep left'
    elif sign == 40:
        return 'Roundabout mandatory'
    elif sign == 41:
        return 'End of no passing'
    elif sign == 42:
        return 'End of no passing by vechiles over 3.5 metric tons'

class_names = ['Speed Limit 20 km/h', 'Speed Limit 30 km/h', 'Speed Limit 50 km/h', 'Speed Limit 60 km/h',
               'Speed Limit 70 km/h', 'Speed Limit 80 km/h', 'End of Speed Limit 80 km/h', 'Speed Limit 100 km/h',
               'Speed Limit 120 km/h', 'No passing', 'No passing for vechiles over 3.5 metric tons', 'Right-of-way at the next intersection',
               'Priority road', 'Yield', 'Stop', 'No vehicles', 'Vechiles over 3.5 metric tons prohibited', 'No entry', 'General caution',
               'Dangerous curve to the left', 'Dangerous curve to the right', 'Double curve', 'Bumpy road', 'Slippery road', 'Road narrows on the right',
               'Road work', 'Traffic signals', 'Pedestrians', 'Children crossing', 'Bicycles crossing', 'Beware of ice/snow', 'Wild animals crossing',
               'End of all speed and passing limits', 'Turn right ahead', 'Turn left ahead', 'Ahead only', 'Go straight or right',
               'Go straight or left', 'Keep right', 'Keep left', 'Roundabout mandatory', 'End of no passing', 'End of no passing by vechiles over 3.5 metric tons']

while cap.isOpened():

    success, imgOrignal = cap.read()

    # PROCESS IMAGE

    img = np.asarray(imgOrignal)
    img = cv2.resize(img, (32, 32))
    img = preprocessing(img)
    img = img.reshape(1, 32, 32, 1)
    cv2.putText(imgOrignal, "CLASS: ", (20, 35), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2, cv2.LINE_AA)

    # PREDICT IMAGE
    predictions = model.predict(img)
    probabilityValue = np.argmax(predictions)

    label = class_names[probabilityValue]

    label = "{}".format(label)

    cv2.putText(imgOrignal, label, (120, 35), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255),2, cv2.LINE_AA)

    cv2.imshow("Result", imgOrignal)
    if cv2.waitKey(10) and 0xFF == ord('q'):
        cap.release()
        cap.destroyAllWindows()
        break
